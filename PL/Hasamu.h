#pragma once
#include "ScrollingBackground.h"
#include "BlockCouple.h"
#include "Item.h"

class Hasamu
{
private:
	enum HASAMU_STATE
	{
		TITLEHASAMU,
		INHASAMU,
		RESULTHASAMU
	};
	HASAMU_STATE m_HasamuState;

public:
	Hasamu();
	~Hasamu();

	void Initialize(ID3D11Device1* device, DirectX::AudioEngine* audio);
	void InitializeRelateWindowSize(UINT width, UINT height);
	void Update(float, DirectX::Keyboard* keyboard, DirectX::Mouse* mouse);
	void Draw(DirectX::SpriteBatch* spriteBatch);
	void Shutdown();

	void PlayBGM();

private:

	UINT m_screenWidth;
	UINT m_screenHeight;

	float m_pressInterval;
	float m_springInterval;

	RECT m_fullscreenRect;
	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_background;
	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_press;
	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_bird;
	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_moving;
	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_block;
	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_titleScene;
	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_resultScene;
	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_ItemS;
	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_ItemB;
	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_ItemP;
	std::unique_ptr<ScrollingBackground> m_scrollingMove;

	DirectX::SimpleMath::Vector2 m_leftPressPos;
	DirectX::SimpleMath::Vector2 m_rightPressPos;
	DirectX::SimpleMath::Vector2 m_topPressPos;
	DirectX::SimpleMath::Vector2 m_bottomPressPos;

	DirectX::SimpleMath::Vector2 m_leftPressOrigin;
	DirectX::SimpleMath::Vector2 m_rightPressOrigin;
	DirectX::SimpleMath::Vector2 m_topPressOrigin;
	DirectX::SimpleMath::Vector2 m_bottomPressOrigin;

	DirectX::SimpleMath::Vector2 m_birdPos;
	DirectX::SimpleMath::Vector2 m_birdOrigin;
	DirectX::SimpleMath::Vector2 m_birdSize;

private:
	// game object properties
	float pressLeftSpeed;
	float pressRightSpeed;
	float pressTopSpeed;
	float pressBottomSpeed;

	float birdSpeed;

	bool pressLeftCollisionFlag;
	bool pressRightCollisionFlag;
	bool pressTopCollisionFlag;
	bool pressBottomCollisionFlag;

	DirectX::SimpleMath::Vector2 birdRatio;
	DirectX::SimpleMath::Vector2 birdSize;



private:
	//fonts
	std::unique_ptr<DirectX::SpriteFont> m_font;
	DirectX::SimpleMath::Vector2 m_scorePos;
	DirectX::SimpleMath::Vector2 m_titlePos;
	DirectX::SimpleMath::Vector2 m_resultPos;

	DirectX::SimpleMath::Vector2 m_scoreOrigin;
	DirectX::SimpleMath::Vector2 m_titleOrigin;
	DirectX::SimpleMath::Vector2 m_resultOrigin;

	int score;

private:
	//sound
	std::unique_ptr<DirectX::SoundEffect> m_backGroundMusic;
	std::unique_ptr<DirectX::SoundEffect> m_birdpiu;
	std::unique_ptr<DirectX::SoundEffectInstance> m_BGMLoop;

private:
	std::vector<std::shared_ptr<BlockCouple>> m_vBlockCouples;

	void MakeBlocks(int);
private:
	//Items
	std::vector<std::shared_ptr<Item>> m_vItems;

	void MakeItems(int, int, int);
	//
	void SetPositions();
};