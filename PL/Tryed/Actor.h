#pragma once
#include "Object.h"

class Actor : public Object
{
public:
	Actor();
	~Actor();
	void Initialize() override;
private:

};