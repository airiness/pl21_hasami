#pragma once

#include "Object.h"

class Block :public Object
{
public:
	Block(const char* filename);
	~Block();


private:
	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_blockTex;
};