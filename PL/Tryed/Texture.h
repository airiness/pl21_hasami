#pragma once
#include "pch.h"
class Texture 
{
public:
	Texture();
	~Texture();

	bool LoadTextureFromFile();
private:
	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_texture;
};