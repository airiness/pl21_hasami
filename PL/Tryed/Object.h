#pragma once
#include "pch.h"
#include "Component.h"

class Object
{
public:
	Object();
	virtual ~Object();

	virtual void Initialize();
	virtual void Update();
	virtual void Uninitialize();

	DirectX::SimpleMath::Vector2 GetPosition();
private:
	DirectX::SimpleMath::Vector2 m_position;
	
};
