#pragma once

class Component
{
public:
	Component();
	virtual ~Component();

	void Initialize();
	virtual void Update() = 0;
private:
	
};