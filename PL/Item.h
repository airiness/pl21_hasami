#pragma once

class Item
{
public:
	static enum ItemType
	{
		CHANGE_SIZE = 1,
		POINT,
		BOOM
	};
public:
	Item(ItemType t)
		:itemSpeed(0),
		mScreenWidth(0),
		mScreenHeight(0),
		itemType(t)
	{

	}

	void Load(ID3D11ShaderResourceView* texture)
	{
		mTexture = texture;
		if (texture)
		{
			Microsoft::WRL::ComPtr<ID3D11Resource> resource;
			texture->GetResource(resource.GetAddressOf());

			D3D11_RESOURCE_DIMENSION dim;
			resource->GetType(&dim);

			if (dim != D3D11_RESOURCE_DIMENSION_TEXTURE2D)
				throw std::exception("Item expects a Texture2D");

			Microsoft::WRL::ComPtr<ID3D11Texture2D> tex2D;
			resource.As(&tex2D);

			D3D11_TEXTURE2D_DESC desc;
			tex2D->GetDesc(&desc);

			mTextureWidth = desc.Width;
			mTextureHeight = desc.Height;

			itemOrigin.x = float(mTextureWidth) / 2.f;
			itemOrigin.y = float(mTextureHeight) / 2.f;

		}
	}

	void SetWindow(UINT screenWidth, UINT screenHeight)
	{
		mScreenWidth = screenWidth;
		mScreenHeight = screenHeight;
		SetProperty();
	}

	void SetProperty()
	{
		itemSize.x = itemSize.y = TOOLS::randf(50.f, 100.f);
		itemRatio.x = itemSize.x / float(mTextureWidth);
		itemRatio.y = itemSize.y / float(mTextureHeight);

		itemSpeed = 100.f + TOOLS::randf(-10.f, 10.f);

		itemPosition.x = TOOLS::randf(100.f, mScreenWidth - 150.f);
		itemPosition.y = TOOLS::randf(-200.f - 2.f * mScreenHeight, -200.f);
	}

	int Update(const float& elapsedTime, const DirectX::SimpleMath::Vector2& bposition, DirectX::SimpleMath::Vector2& bsize , int& score)
	{

		itemPosition.y += (itemSpeed * elapsedTime);
		if ((itemPosition.y - itemSize.y) > mScreenHeight)
		{
			SetProperty();
		}
		int ret = 0;
		if (
			(abs(bposition.x - itemPosition.x) < ((bsize.x + itemSize.x) / 2.f))
			&& (abs(bposition.y - itemPosition.y) < ((bsize.y + itemSize.y) / 2.f))
			)
		{
			if (itemType == ItemType::BOOM)
			{
				ret = 1;
			}
			else if (itemType == ItemType::CHANGE_SIZE)
			{
				//float r = TOOLS::randf(0.8f, 2.2f);
				//bsize *= r;
				//some bugs
				score += 2;
				ret = 2;
			}
			else if (itemType == ItemType::POINT)
			{
				score++;
				ret = 3;
			}
			SetProperty();
		}


		return ret;

	}

	void Draw(DirectX::SpriteBatch *spriteBatch)
	{
		spriteBatch->Draw(mTexture.Get(), itemPosition, nullptr, DirectX::Colors::White, 0.f, itemOrigin, itemRatio);
	}

private:

	DirectX::SimpleMath::Vector2 itemPosition;
	DirectX::SimpleMath::Vector2 itemOrigin;
	DirectX::SimpleMath::Vector2 itemSize;
	DirectX::SimpleMath::Vector2 itemRatio;

	int mScreenWidth;
	int mScreenHeight;
	int mTextureWidth;
	int mTextureHeight;

	float itemSpeed;
	ItemType itemType;
	bool isUse;

	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> mTexture;
};