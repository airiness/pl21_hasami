#include "pch.h"
#include "Hasamu.h"

Hasamu::Hasamu()
{
}

Hasamu::~Hasamu()
{
}

void Hasamu::Initialize(ID3D11Device1* device, DirectX::AudioEngine* audio)
{
	m_HasamuState = TITLEHASAMU;
	Microsoft::WRL::ComPtr<ID3D11Resource> resource;

	DX::ThrowIfFailed(DirectX::CreateWICTextureFromFile(device, L"Assets/Textures/movingBack.png", nullptr, m_moving.ReleaseAndGetAddressOf()));
	m_scrollingMove = std::make_unique<ScrollingBackground>();
	m_scrollingMove->Load(m_moving.Get());

	DX::ThrowIfFailed(DirectX::CreateWICTextureFromFile(device, L"Assets/Textures/background.png", nullptr, m_background.ReleaseAndGetAddressOf()));
	DX::ThrowIfFailed(DirectX::CreateWICTextureFromFile(device, L"Assets/Textures/Title.png", nullptr, m_titleScene.ReleaseAndGetAddressOf()));
	DX::ThrowIfFailed(DirectX::CreateWICTextureFromFile(device, L"Assets/Textures/Result.png", nullptr, m_resultScene.ReleaseAndGetAddressOf()));



	DX::ThrowIfFailed(DirectX::CreateWICTextureFromFile(device, L"Assets/Textures/press.png", resource.GetAddressOf(), m_press.ReleaseAndGetAddressOf()));
	Microsoft::WRL::ComPtr<ID3D11Texture2D> texture;
	DX::ThrowIfFailed(resource.As(&texture));
	CD3D11_TEXTURE2D_DESC textureDesc;
	texture->GetDesc(&textureDesc);

	m_leftPressOrigin.x = m_rightPressOrigin.x = m_topPressOrigin.x = m_bottomPressOrigin.x = float(textureDesc.Width);
	m_leftPressOrigin.y = m_rightPressOrigin.y = m_topPressOrigin.y = m_bottomPressOrigin.y = float(textureDesc.Height / 2);


	DX::ThrowIfFailed(DirectX::CreateWICTextureFromFile(device, L"Assets/Textures/bird.png", resource.GetAddressOf(), m_bird.ReleaseAndGetAddressOf()));
	DX::ThrowIfFailed(resource.As(&texture));
	texture->GetDesc(&textureDesc);

	m_birdOrigin.x = float(textureDesc.Width / 2);
	m_birdOrigin.y = float(textureDesc.Height / 2);

	birdSize.x = m_birdSize.x = float(textureDesc.Width);
	birdSize.y = m_birdSize.y = float(textureDesc.Height);

	birdRatio.x = 1.f;
	birdRatio.y = 1.f;

	pressLeftCollisionFlag = false;
	pressRightCollisionFlag = false;
	pressTopCollisionFlag = false;
	pressBottomCollisionFlag = false;

	//font 
	m_font = std::make_unique<DirectX::SpriteFont>(device, L"Assets/Fonts/Ravie.spritefont");

	score = 0;
	m_scoreOrigin = { 0.f,0.f };
	m_titleOrigin = m_resultOrigin = { 0.f,16.f };

	//sound
	m_birdpiu = std::make_unique<DirectX::SoundEffect>(audio, L"Assets/Audios/birdpiu.wav");

	m_backGroundMusic = std::make_unique<DirectX::SoundEffect>(audio, L"Assets/Audios/BGMdddd.wav");
	m_BGMLoop = m_backGroundMusic->CreateInstance();
	m_BGMLoop->Play(true);

	//blockcouples
	DX::ThrowIfFailed(DirectX::CreateWICTextureFromFile(device, L"Assets/Textures/block.png", resource.GetAddressOf(), m_block.ReleaseAndGetAddressOf()));
	MakeBlocks(4);

	//items
	DX::ThrowIfFailed(DirectX::CreateWICTextureFromFile(device, L"Assets/Textures/ItemB.png", resource.GetAddressOf(), m_ItemB.ReleaseAndGetAddressOf()));
	DX::ThrowIfFailed(DirectX::CreateWICTextureFromFile(device, L"Assets/Textures/ItemP.png", resource.GetAddressOf(), m_ItemP.ReleaseAndGetAddressOf()));
	DX::ThrowIfFailed(DirectX::CreateWICTextureFromFile(device, L"Assets/Textures/ItemS.png", resource.GetAddressOf(), m_ItemS.ReleaseAndGetAddressOf()));
	MakeItems(2, 4, 2);

}

void Hasamu::InitializeRelateWindowSize(UINT width, UINT height)
{

	m_fullscreenRect.left = 0;
	m_fullscreenRect.top = 0;
	m_fullscreenRect.right = width;
	m_fullscreenRect.bottom = height;

	m_screenWidth = width;
	m_screenHeight = height;
	m_pressInterval = 50.0f;
	m_springInterval = 100.f;
	m_birdPos = { width / 2.f,height / 2.f };

	//font pos
	m_titlePos = { width / 2.f,height / 2.f - 200.f };
	m_resultPos = { width / 2.f,height / 2.f };

	m_scrollingMove->SetWindow(width, height);

	for (auto & b : m_vBlockCouples)
	{
		b->SetWindow(width, height);
	}

	for (auto & i:m_vItems)
	{
		i->SetWindow(width, height);
	}

	SetPositions();

	//score text position
	m_scorePos = { 0.f,0.f };
}

void Hasamu::Update(float elapsedTime, DirectX::Keyboard* keyboard, DirectX::Mouse* mouse)
{

	auto kb = keyboard->GetState();
	if (m_HasamuState == TITLEHASAMU)
	{
		if (kb.Space)
		{
			m_HasamuState = INHASAMU;
		}
	}
	else if (m_HasamuState == INHASAMU)
	{
		static const float PRESSED_SPEED = 600.0f;
		static const float PRESS_BACK_SPEED = 600.0f;
		static const float MIN_SIZE = 32.0f;


		m_leftPressPos.y = m_birdPos.y;
		m_rightPressPos.y = m_birdPos.y;
		m_topPressPos.x = m_birdPos.x;
		m_bottomPressPos.x = m_birdPos.x;

		pressLeftSpeed = pressTopSpeed = -600.f;
		pressRightSpeed = pressBottomSpeed = 600.f;

		if (kb.Left)
		{
			pressLeftSpeed = PRESSED_SPEED;
		}
		if (kb.Right)
		{
			pressRightSpeed = -PRESSED_SPEED;
		}
		if (kb.Up)
		{
			pressTopSpeed = PRESSED_SPEED;
		}
		if (kb.Down)
		{
			pressBottomSpeed = -PRESSED_SPEED;
		}

		if (kb.A)
		{
			pressLeftSpeed = -PRESS_BACK_SPEED;
		}
		if (kb.D)
		{
			pressRightSpeed = PRESS_BACK_SPEED;
		}
		if (kb.W)
		{
			pressTopSpeed = -PRESS_BACK_SPEED;
		}
		if (kb.S)
		{
			pressBottomSpeed = PRESS_BACK_SPEED;
		}

		//find the position
		m_leftPressPos.x += (pressLeftSpeed * elapsedTime);
		m_rightPressPos.x += (pressRightSpeed * elapsedTime);
		m_topPressPos.y += (pressTopSpeed * elapsedTime);
		m_bottomPressPos.y += (pressBottomSpeed * elapsedTime);

		//check collision

		pressLeftCollisionFlag = (m_leftPressPos.x >= m_birdPos.x - birdSize.x / 2.f) ? true : false;

		pressRightCollisionFlag = (m_rightPressPos.x <= m_birdPos.x + birdSize.x / 2.f) ? true : false;

		pressTopCollisionFlag = (m_topPressPos.y >= m_birdPos.y - birdSize.y / 2.f) ? true : false;

		pressBottomCollisionFlag = (m_bottomPressPos.y <= m_birdPos.y + birdSize.y / 2.f) ? true : false;

		if (pressLeftCollisionFlag && pressRightCollisionFlag && pressTopCollisionFlag && pressBottomCollisionFlag)
		{
			pressLeftSpeed = 0.0f;
			pressRightSpeed = 0.0f;
			pressTopSpeed = 0.0f;
			pressBottomSpeed = 0.0f;
			m_rightPressPos.x = m_birdPos.x + birdSize.x / 2.f;
			m_leftPressPos.x = m_birdPos.x - birdSize.x / 2.f;
			m_bottomPressPos.y = m_birdPos.y + birdSize.y / 2.f;
			m_topPressPos.y = m_birdPos.y - birdSize.y / 2.f;
		}
		else
		{
			if (pressLeftCollisionFlag && pressRightCollisionFlag)
			{
				if (m_rightPressPos.x - m_leftPressPos.x < MIN_SIZE)
				{
					m_rightPressPos.x = m_birdPos.x + MIN_SIZE / 2.f;
					m_leftPressPos.x = m_birdPos.x - MIN_SIZE / 2.f;
					pressLeftSpeed = 0.0f;
					pressRightSpeed = 0.0f;
					birdSize.x = MIN_SIZE;
				}
				else
				{
					birdSize.x = m_rightPressPos.x - m_leftPressPos.x;
				}
				birdSize.y = m_birdSize.y * 2.f - birdSize.x;
				//
				if ((m_birdPos.y - birdSize.y / 2.f) < m_springInterval)
				{
					m_birdPos.y = birdSize.y / 2.f + m_springInterval;
				}
				else if ((m_birdPos.y + birdSize.y / 2.f) > (m_screenHeight - m_springInterval))
				{
					m_birdPos.y = m_screenHeight - birdSize.y / 2.f - m_springInterval;
				}
				//
			}
			else if (pressLeftCollisionFlag && !pressRightCollisionFlag)
			{
				m_birdPos.x = m_leftPressPos.x + birdSize.x / 2.f;
				if ((m_birdPos.x + birdSize.x / 2.f) > (m_screenWidth - m_springInterval))
				{
					pressLeftSpeed = 0.f;
					m_birdPos.x = m_screenWidth - birdSize.x / 2.f - m_springInterval;
					m_leftPressPos.x = m_screenWidth - m_springInterval - birdSize.x;
				}
			}
			else if (!pressLeftCollisionFlag && pressRightCollisionFlag)
			{
				m_birdPos.x = m_rightPressPos.x - birdSize.x / 2.f;
				if ((m_birdPos.x - birdSize.x / 2.f) < m_springInterval)
				{
					pressRightSpeed = 0.f;
					m_birdPos.x = birdSize.x / 2.f + m_springInterval;
					m_rightPressPos.x = m_springInterval + birdSize.x;
				}
			}


			if (pressTopCollisionFlag && pressBottomCollisionFlag)
			{
				if (m_bottomPressPos.y - m_topPressPos.y < MIN_SIZE)
				{
					m_bottomPressPos.y = m_birdPos.y + MIN_SIZE / 2.f;
					m_topPressPos.y = m_birdPos.y - MIN_SIZE / 2.f;
					pressTopSpeed = 0.0f;
					pressBottomSpeed = 0.0f;
					birdSize.y = MIN_SIZE;
				}
				else
				{
					birdSize.y = m_bottomPressPos.y - m_topPressPos.y;
				}
				birdSize.x = m_birdSize.x * 2.f - birdSize.y;

				if ((m_birdPos.x + birdSize.x / 2.f) > (m_screenWidth - m_springInterval))
				{
					m_birdPos.x = m_screenWidth - birdSize.x / 2.f - m_springInterval;
				}
				else if ((m_birdPos.x - birdSize.x / 2.f) < m_springInterval)
				{
					m_birdPos.x = birdSize.x / 2.f + m_springInterval;
				}
			}
			else if (pressTopCollisionFlag && !pressBottomCollisionFlag)
			{
				m_birdPos.y = m_topPressPos.y + birdSize.y / 2.f;
				if ((m_birdPos.y + birdSize.y / 2.f) > (m_screenHeight - m_springInterval))
				{
					pressTopSpeed = 0.f;
					m_birdPos.y = m_screenHeight - birdSize.y / 2.f - m_springInterval;
					m_topPressPos.y = m_screenHeight - m_springInterval - birdSize.y;
				}
			}
			else if (!pressTopCollisionFlag && pressBottomCollisionFlag)
			{
				m_birdPos.y = m_bottomPressPos.y - birdSize.y / 2.f;
				if ((m_birdPos.y - birdSize.y / 2.f) < m_springInterval)
				{
					pressBottomSpeed = 0.f;
					m_birdPos.y = birdSize.y / 2.f + m_springInterval;
					m_bottomPressPos.y = m_springInterval + birdSize.y;
				}
			}
		}

		//adjust position
		if (m_leftPressPos.x < m_pressInterval)
		{
			m_leftPressPos.x = m_pressInterval;
		}
		if (m_rightPressPos.x > (m_screenWidth - m_pressInterval))
		{
			m_rightPressPos.x = (m_screenWidth - m_pressInterval);
		}
		if (m_topPressPos.y < m_pressInterval)
		{
			m_topPressPos.y = m_pressInterval;
		}
		if (m_bottomPressPos.y > (m_screenHeight - m_pressInterval))
		{
			m_bottomPressPos.y = (m_screenHeight - m_pressInterval);
		}
		birdRatio = birdSize / m_birdSize;

		bool result = false;
		for (auto & b : m_vBlockCouples)
		{
			result = b->Update(elapsedTime, m_birdPos, birdSize);
			if (result)
			{
				m_HasamuState = RESULTHASAMU;
			}
		}
		int ret;
		for (auto & i:m_vItems)
		{
			ret = i->Update(elapsedTime, m_birdPos, birdSize ,score);
			if (ret == 1)
			{
				m_HasamuState = RESULTHASAMU;
			}
			else if (ret==2||ret==3)
			{
				m_birdpiu->Play();
			}
		}

		m_scrollingMove->Update(elapsedTime * 200);

	}
	else if (m_HasamuState == RESULTHASAMU)
	{
		if (kb.Space)
		{
			m_HasamuState = TITLEHASAMU;
			//init position and points
			score = 0;
			m_birdPos = { m_screenWidth / 2.f,m_screenHeight / 2.f };
			birdSize = m_birdSize;
			SetPositions();

		}
	}
}

void Hasamu::Draw(DirectX::SpriteBatch *spriteBatch)
{
	if (m_HasamuState == TITLEHASAMU)
	{
		spriteBatch->Draw(m_titleScene.Get(), m_fullscreenRect);
		std::wstring output = std::wstring(L"Hasamu Bird");
		m_font->DrawString(spriteBatch, output.c_str(), m_titlePos, DirectX::Colors::White, 0.f, { output.length() * 18.f ,m_titleOrigin.y }, 2.2f);

	}
	else if (m_HasamuState == INHASAMU)
	{
		m_scrollingMove->Draw(spriteBatch);
		spriteBatch->Draw(m_background.Get(), m_fullscreenRect);


		for (auto & b : m_vBlockCouples)
		{
			b->Draw(spriteBatch);
		}

		for (auto & i : m_vItems)
		{
			i->Draw(spriteBatch);
		}

		spriteBatch->Draw(m_bird.Get(), m_birdPos, nullptr, DirectX::Colors::White, 0.f, m_birdOrigin, birdRatio);
		spriteBatch->Draw(m_press.Get(), m_leftPressPos, nullptr, DirectX::Colors::White, 0.f, m_leftPressOrigin);
		spriteBatch->Draw(m_press.Get(), m_rightPressPos, nullptr, DirectX::Colors::White, DirectX::XM_PI, m_rightPressOrigin);
		spriteBatch->Draw(m_press.Get(), m_topPressPos, nullptr, DirectX::Colors::White, DirectX::XM_PI / 2.f, m_topPressOrigin);
		spriteBatch->Draw(m_press.Get(), m_bottomPressPos, nullptr, DirectX::Colors::White, DirectX::XM_PI * 1.5f, m_bottomPressOrigin);

		std::wstring output = std::wstring(L"score:") + std::to_wstring(score) + std::wstring(L"Pts");
		m_font->DrawString(spriteBatch, output.c_str(), m_scorePos, DirectX::Colors::White, 0.f, m_scoreOrigin);
	}
	else if (m_HasamuState == RESULTHASAMU)
	{
		spriteBatch->Draw(m_resultScene.Get(), m_fullscreenRect);
		std::wstring output1 = std::wstring(L"score:") + std::to_wstring(score) + std::wstring(L"Pts");
		std::wstring output2 = std::wstring(L"GAME OVER");
		DirectX::SimpleMath::Vector2 posTmp = { m_resultPos.x ,m_resultPos.y + 200.f };
		m_font->DrawString(spriteBatch, output1.c_str(), posTmp, DirectX::Colors::White, 0.f, { output1.length() * 20.f ,m_resultOrigin.y }, 1.2f);
		posTmp.y -= 300.f;
		m_font->DrawString(spriteBatch, output2.c_str(), posTmp, DirectX::Colors::White, 0.f, { output2.length() * 20.f ,m_resultOrigin.y }, 2.f);
	}

}

void Hasamu::Shutdown()
{
	m_press.Reset();
	m_bird.Reset();
	m_background.Reset();
	m_moving.Reset();
	m_block.Reset();
	m_font.reset();
	m_BGMLoop.reset();
}

void Hasamu::PlayBGM()
{
	m_BGMLoop->Play(true);
}

void Hasamu::MakeBlocks(int num)
{
	for (size_t i = 0; i < num; i++)
	{
		std::shared_ptr<BlockCouple> blockcouple = std::make_shared<BlockCouple>();
		blockcouple->Initialize(m_block.Get());
		m_vBlockCouples.push_back(blockcouple);
	}
}

void Hasamu::MakeItems(int numB, int numP, int numS)
{
	for (size_t i = 0; i < numB; i++)
	{
		std::shared_ptr<Item> item = std::make_shared<Item>(Item::ItemType::BOOM);
		item->Load(m_ItemB.Get());
		m_vItems.push_back(item);
	}
	for (size_t i = 0; i < numP; i++)
	{
		std::shared_ptr<Item> item = std::make_shared<Item>(Item::ItemType::POINT);
		item->Load(m_ItemP.Get());
		m_vItems.push_back(item);
	}
	for (size_t i = 0; i < numS; i++)
	{
		std::shared_ptr<Item> item = std::make_shared<Item>(Item::ItemType::CHANGE_SIZE);
		item->Load(m_ItemS.Get());
		m_vItems.push_back(item);
	}
}

void Hasamu::SetPositions()
{
	m_leftPressPos = { m_pressInterval,m_birdPos.y };
	m_rightPressPos = { m_screenWidth - m_pressInterval,m_birdPos.y };
	m_topPressPos = { m_birdPos.x,m_pressInterval };
	m_bottomPressPos = { m_birdPos.x,m_screenHeight - m_pressInterval };
	float interval = 600.f;
	for (size_t i = 0; i < m_vBlockCouples.size(); i++)
	{
		m_vBlockCouples[i]->SetPosition(TOOLS::randf(float(m_screenWidth + 200), float(m_screenWidth + 230)) + interval * i);
	}
	for (auto & i:m_vItems)
	{
		i->SetProperty();
	}
}
