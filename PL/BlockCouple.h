#pragma once
class BlockCouple
{
public:
	BlockCouple():
		blockCoupleDistance(0),
		blockCoupleSpeed(0),
		mScreenWidth(0),
		mScreenHeight(0)
	{}

	void Initialize(ID3D11ShaderResourceView* texture)
	{
		mTexture = texture;
		if (texture)
		{
			Microsoft::WRL::ComPtr<ID3D11Resource> resource;
			texture->GetResource(resource.GetAddressOf());

			D3D11_RESOURCE_DIMENSION dim;
			resource->GetType(&dim);

			if (dim != D3D11_RESOURCE_DIMENSION_TEXTURE2D)
				throw std::exception("blockcouple expects a Texture2D");

			Microsoft::WRL::ComPtr<ID3D11Texture2D> tex2D;
			resource.As(&tex2D);

			D3D11_TEXTURE2D_DESC desc;
			tex2D->GetDesc(&desc);

			mTextureWidth = desc.Width;
			mTextureHeight = desc.Height;

			block1Origin = { float(mTextureWidth) / 2.f, 0.f};
			block2Origin = { float(mTextureWidth) / 2.f, float(mTextureHeight)  };

			blockCoupleSpeed = 100.f;// TOOLS::randf(180.f, 200.f);

		}
	}

	void SetWindow(UINT screenWidth, UINT screenHeight)
	{
		mScreenWidth = screenWidth;
		mScreenHeight = screenHeight;
		block1Pos.y = 0.f;
		block2Pos.y = mScreenHeight;
		RandomBlockSize();
	}

	void SetPosition( float x)
	{
		block1Pos.x = block2Pos.x = x;
		RandomBlockSize();
	}

	bool Update(const float& elapsedTime, const DirectX::SimpleMath::Vector2& bposition, const DirectX::SimpleMath::Vector2& bsize)
	{
		blockCoupleSpeed += 0.001f;
		block1Pos.x -= blockCoupleSpeed * elapsedTime;
		block2Pos.x = block1Pos.x;
		if (block1Pos.x <( -block1Size.x / 2.f))
		{
			block1Pos.x = block2Pos.x = TOOLS::randf(float(mScreenWidth + 500), float(mScreenWidth + 520));
			RandomBlockSize();
		}

		if (
			((bposition.y - (bsize.y / 2.f) < block1Size.y)
				|| (bposition.y + (bsize.y / 2.f) > mScreenHeight - block2Size.y))
			&& (abs(bposition.x-block1Pos.x) < (bsize.x /2.f + block1Size.x /2.f))
				)
		{
			return true;
		}

		return false;
	}

	void Draw(DirectX::SpriteBatch *spriteBatch)
	{
		spriteBatch->Draw(mTexture.Get(), block1Pos, nullptr, DirectX::Colors::White, 0.f, block1Origin, block1Ratio);
		spriteBatch->Draw(mTexture.Get(), block2Pos, nullptr, DirectX::Colors::White, 0.f, block2Origin, block2Ratio);
	}

private:

	void RandomBlockSize()
	{
		block1Size.x = block2Size.x = TOOLS::randf(150.f, 200.f);
		block1Size.y = TOOLS::randf(100.f, 700.f);
		blockCoupleDistance = TOOLS::randf(100.f, 200.f);
		block2Size.y = mScreenHeight - block1Size.y - blockCoupleDistance;

		block1Ratio.x = block1Size.x / mTextureWidth;
		block1Ratio.y = block1Size.y / mTextureHeight;

		block2Ratio.x = block2Size.x / mTextureWidth;
		block2Ratio.y = block2Size.y / mTextureHeight;
	}

	DirectX::SimpleMath::Vector2 block1Pos;
	DirectX::SimpleMath::Vector2 block2Pos;

	DirectX::SimpleMath::Vector2 block1Origin;
	DirectX::SimpleMath::Vector2 block2Origin;

	DirectX::SimpleMath::Vector2 block1Size;
	DirectX::SimpleMath::Vector2 block2Size;

	DirectX::SimpleMath::Vector2 block1Ratio;
	DirectX::SimpleMath::Vector2 block2Ratio;

	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> mTexture;
	int	mScreenWidth;
	int mScreenHeight;
	int mTextureWidth;
	int mTextureHeight;


	float blockCoupleDistance;
	float blockCoupleSpeed;

};
